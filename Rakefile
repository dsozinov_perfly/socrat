require 'rubygems'
require 'bundler/setup'
require_relative "vk_crawler"
require_relative "pipeline"

namespace :vk do
    fields_profile_user = "nickname,screen_name,sex,bdate,city,country,timezone,photo_50,photo_100,photo_200_orig,has_mobile,contacts,education,online,counters,relation,last_seen,status,can_write_private_message,can_see_all_posts,can_see_audio,can_post,universities,schools,verified,place"
    fields_group = "city,country,description,wiki_page,members_count,counters,can_post,can_see_all_posts,activity,status,fixed_post,verified,contacts,links,place"
    task :friends, [:token, :snapshot_id, :user_id] do |t, args|
        token = args.token
        user_id = args.user_id
        snapshot_id = args.snapshot_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.friends_get({"uid" => user_id}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", ".")) 
            pipe.handle(result)
        end    
    end    
    task :current_profile, [:token, :snapshot_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.users_get({"fields" => fields_profile_user}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :profile, [:token, :snapshot_id, :user_ids] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        user_ids = args.user_ids
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.users_get({"uids" => user_ids, "fields" => fields_profile_user}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :groups, [:token, :snapshot_id, :user_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        user_id = args.user_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.groups_get({"uid" => user_id}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :groups_info, [:token, :snapshot_id, :group_ids] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        group_ids = args.group_ids
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.groups_getById({"gids" => group_ids, "fields" => fields_group}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end    
    task :documents, [:token, :snapshot_id, :owner_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        owner_id = args.owner_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.docs_get({"oid" => owner_id}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :notes, [:token, :snapshot_id, :user_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        user_id = args.user_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.notes_get({"uid" => user_id}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :note_comments, [:token, :snapshot_id, :user_id, :note_id, :offset, :count] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        user_id = args.user_id
        note_id = args.note_id
        offset = args.offset
        count = args.count
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.notes_getComments({"owner_id" => user_id, "nid" => note_id, "offset" => offset, "count" => count}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :albums, [:token, :snapshot_id, :user_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        user_id = args.user_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.photos_getAlbums({"uid" => user_id, "need_covers" => "1"}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
    task :photos, [:token, :snapshot_id, :user_id, :album_id] do |t, args|
        token = args.token
        snapshot_id = args.snapshot_id
        album_id = args.album_id
        user_id = args.user_id
        crawler = VkCrawler.new("https://api.vk.com/method/", token)
        crawler.photos_get({"uid" => user_id, "aid" => album_id, "extended" => "1"}) do |result|
            pipe = ProfileSnapshotTransformer.new(snapshot_id) >> MongoStore.new("socrat", t.name.to_s.gsub(":", "."))
            pipe.handle(result)
        end
    end
end    
