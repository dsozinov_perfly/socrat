#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'
require 'json'
require 'httpclient'
require 'uri'


class VkCrawler

    def initialize(url, token)
        @url = url
        @http = HTTPClient.new()
        @token = token
    end    

    def method_missing(meth, *args, &block)
        vk_method_name = meth.to_s.gsub("_",".")
        block.call(JSON.parse(@http.get(URI.join(@url, vk_method_name), args.first.update({"access_token" => @token})).content))
    end    
end    
